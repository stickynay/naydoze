package app.com.study.stickyny.naydoze;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by NN on 2016-07-05.
 */
public class PollService extends IntentService {
    private static final String TAG = "PollService";

    private static String mInfo = "";

    public String getInfo() {
        return mInfo;
    }

    private void setInfo(String info) {
        mInfo = info;
    }

    private static final int POLL_INTERVAL = 1000 * 60 * 5; //5 min

    public PollService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.i(TAG, "Received an intent: " + intent);
        Log.i(TAG, getCurrentTime());
        Log.i(TAG, "Network status: " + isNetworkPossible());

        setInfo(getInfo() + "Network status: " + isNetworkPossible() + "(" + getCurrentTime() + ")\n");


        if(isExternalStorageWritable()) {
            saveInfo(getInfo());
        }

    }

    private String getCurrentTime() {
        long now = System.currentTimeMillis();
        Date date = new Date(now);
        SimpleDateFormat CurDateFormat = new SimpleDateFormat("yyyy/MM/dd @HH:mm");
        return CurDateFormat.format(date);
    }

    private boolean isNetworkPossible() {
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        NetworkReceiver receiver = new NetworkReceiver();
        registerReceiver(receiver, filter);
        boolean status = receiver.isNetworkStatus();
        unregisterReceiver(receiver);
        return status;
    }

    private void saveInfo(String info) {
        String path = "/sdcard/";
        String fileName = "doze_info.txt";

        File file = new File(path + fileName);

        FileOutputStream fos = null;

        try {
            fos = new FileOutputStream(file);
            fos.write((info).getBytes());
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if(Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    public static void setServiceAlarm(Context context, boolean isOn) {
        Intent intent = new Intent(context, PollService.class);
        PendingIntent pendingIntent = PendingIntent.getService(context, 0, intent, 0);

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        if(isOn) {
            Toast.makeText(context, "Start Service", Toast.LENGTH_SHORT).show();
            alarmManager.setRepeating(AlarmManager.RTC, System.currentTimeMillis(), POLL_INTERVAL, pendingIntent);
        } else {
            Toast.makeText(context, "Stop Service", Toast.LENGTH_SHORT).show();
            alarmManager.cancel(pendingIntent);
            pendingIntent.cancel();
        }
    }
}
