package app.com.study.stickyny.naydoze;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by NN on 2016-07-05.
 */
public class NetworkReceiver extends BroadcastReceiver {

    private boolean networkStatus;

    public boolean isNetworkStatus() {
        return networkStatus;
    }

    private void setNetworkStatus(boolean networkStatus) {
        this.networkStatus = networkStatus;
    }

    public NetworkReceiver() {
        super();
        networkStatus = false;
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        if(intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mobile = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            NetworkInfo wifi = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            if((mobile != null && mobile.isConnected()) || (wifi != null && wifi.isConnected())) {
                setNetworkStatus(true);
            }
        }
    }
}
